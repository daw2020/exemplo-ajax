const express = require('express')
const app = express()

app.use(express.urlencoded({ extended: true }))

app.use(express.static('public'))


app.get('/', (req, res) => {
    res.sendFile(__dirname + "/public/html/index.html")
})

app.get('/testAjax', (req, res) => {
    res.json({
        pessoas: [{
            nome: "Maria",
            idade: 25
        }, {
            nome: "Alex",
            idade: 30
        }]
    }).end()
})

app.listen(3000);